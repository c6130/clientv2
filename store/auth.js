export const state = () => {
  return {
    user: null,
  }
}

export const mutations = {
  setUser(state, user) {
    state.user = user
  },
}

export const actions = {
  async checkAuth({ $axios, commit }) {
    await new Promise((resolve, reject) => {
      this.$axios
        .get('/api/login/is-auth')
        .then((user) => {
          commit('setUser', user)
          resolve(user)
        })
        .catch((err) => {
          console.log(err)
          window.location.href = '/auth/login'
        })
    })
  },

  logout() {
    deleteAllCookies()
    this.$router.push('/auth/login')
  },
}

function deleteAllCookies() {
  const cookies = document.cookie.split(';')
  for (let i = 0; i < cookies.length; i++) {
    const cookie = cookies[i]
    const eqPos = cookie.indexOf('=')
    const name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie
    document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:00 GMT'
  }
}
